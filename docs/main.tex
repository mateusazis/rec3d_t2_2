\documentclass[12pt]{article}

\usepackage{graphicx}
\usepackage{subfigure}
\usepackage{sbc-template}
\usepackage{url}
\usepackage[utf8]{inputenc} %for special chars
\usepackage[portuguese]{babel} %for correct hypenization
\usepackage{float}
\usepackage{textcomp}
\usepackage{amsmath}

\sloppy

\title{Reconstrução Geométrica por Triangulação Passiva}

\author{Mateus C. Azis }


\address{Instituto de Computação -- Universidade Federal Fluminense
  (UFF)\\
  \email{mazis@ic.uff.br}
}

\begin{document} 

\maketitle

\begin{abstract}
  This is a report about the 3D reconstruction assignment aimed at reconstructing the geometry of a scene using passive triangulation. We present the techniques used, the results, a discussion and the implementation difficulties.
\end{abstract}

\begin{resumo}
  Este é um relatório sobe o trabalho de reconstrução 3D que tinha como objetivo reconstruir a geometria de uma cena usando triangulação passiva. Apresentamos as técnicas usadas, os resultados, uma discussão e as dificuldades de implementação.
\end{resumo}

\section{Descrição do problema}

Dadas duas ou mais fotografias de uma cena, o problema consistem em tentar montar uma reprojeção em 3D dos pontos projetados nas imagens.

Seja \textbf{X} um ponto numa cena, fotografado por uma câmera de centro de projeção localizado no ponto \textbf{C}. O pixel do plano de imagem dessa câmera no qual \textbf{X} é projetado depende dos parâmetros intrínsicos e extrínsicos da câmera. Os parâmetros intrínsicos dependem do processo e dos materiais utilizados na fabricação da câmera. A matriz de parâmetro intrínsicos tem a forma
\begin{equation}
K = \begin{bmatrix}
f m_{x} & s & p_{x}\\ 
0 & fm_{y} & p_{y}\\ 
0 & 0 & 1
\end{bmatrix},
\end{equation}
onde $f$ é a \textbf{distância focal}, $p_{x}$ e $p_{y}$ são as coordenadas do \textbf{ponto principal}, $s$ é o fator de cisalhamento e $m_{x}$ e $m_{y}$ são o número de pixels por unidade de medida em $x$ e $y$, respectivamente.

Além disso, há os parâmetros extrínsicos. Eles dependem do posicionamento da câmera na cena no exato momento em que a fotografia é feita. Esses parâmetros são expressos por uma matriz de rotação \textbf{R} e por um vetor posição \textbf{t}.

Por fim, a projeção se dá por uma matriz 3x4 de formato
\begin{equation}
P = K R \begin{bmatrix}
I & t
\end{bmatrix},
\end{equation} que transforma pontos da cena em pontos no plano de imagem, conforme a equação

\begin{equation}
\label{eq:projection}
x = P X.
\end{equation}

De modo análogo, outra câmera com centro de projeção em \textbf{C\textquotesingle} terá seus próprios parâmetros e, portanto, suas próprias matrizes K\textquotesingle, R\textquotesingle, t\textquotesingle e  P\textquotesingle. Nessa câmera, \textbf{X} é projetado no ponto \textbf{x\textquotesingle}.


A projeção do centro de projeção de uma imagem em outra é chamado \textbf{epipolo}. Assim sendo, a projeção de \textbf{C} na segunda imagem é o epipolo \textbf{e\textquotesingle} e, a de \textbf{C\textquotesingle} na primeira, \textbf{e}. Entre as duas imagens, podemos estabelecer uma relação expressa pela matriz fundamental \textbf{F}. Essa matriz mapeia pontos de uma imagem em linhas na outra imagem. Essas linhas, chamadas \textbf{linhas epipolares}, passam pela projeção do ponto \textbf{X} na segunda imagem e pelo epipolo. Por exemplo, para a projeção \textbf{x} na primeira imagem, temos a linha epipolar
\begin{equation}
l = Fx,
\end{equation}
que passa pelo epipolo \textbf{e\textquotesingle} e pela projeção \textbf{x\textquotesingle}. Portanto, 
\begin{equation}\label{eq:f}
x'^{T}l = x'^{T}Fx = 0.
\end{equation}

De forma análoga, temos a linha epipolar na primeira imagem:
\begin{equation}
l' = x'^{T}F.
\end{equation}

A primeira parte do problema, então, é encontrar a matriz fundamental. Em seguida, consideramos as matrizes de projeção como
\begin{equation}\label{eq:p}
P = \begin{bmatrix}
I & 0
\end{bmatrix}
\end{equation} e \begin{equation}\label{eq:p_}
P'  =
\begin{bmatrix}
 \left [ e \right ]_{x} F + e'v^{t}] & \lambda e'
\end{bmatrix}.
\end{equation}

Usamo-nas para estimar as coordenadas do ponto da cena \textbf{X}.

\section{Origem das imagens}

Utilizamos imagens vindas de duas fontes. A primeira foram os \textit{datasets} fornecidos pelo grupo de pesquisa de Visão Computacional da Universidade de Middlebury. Esses dados já incluíam os valores dos parâmetros intrínsicos e extrínsicos das câmeras utilizadas.

Também foram utilizadas fotos tiradas pelo aluno. Nesse caso, foram utilizadas rotinas da biblioteca OpenCV para corrigir a distorção radial, estimar os parâmetros intrínsicos e calcular similaridades utilizando ASIFT.

\section{Estratégias utilizadas}

Foram utilizadas três estratégias diferentes para encontrar a matriz fundamental \textbf{F}. A primeira consiste na marcação de 8 pares de pontos correspondentes em ambas as imagens e expandir a Equação~\ref{eq:f} com $x = (x, y, t)^{T}$ e $x\textquotesingle = (x\textquotesingle, y\textquotesingle, z\textquotesingle)^{T}$ para obter um produto interno de dois vetores na forma
\begin{equation}
(x\textquotesingle x, x\textquotesingle y, x\textquotesingle, y\textquotesingle x, y\textquotesingle y, y\textquotesingle, x, y, 1) \cdot f = 0.
\end{equation}

Então, preenchemos 8 linhas de uma matriz \textbf{A} com os coeficientes de cada par de pontos e resolvemos \textbf{F} resolvendo
\begin{equation}
A \cdot f = 0.
\end{equation}

Em seguida, forçamos que \textbf{F} tenha posto 3. Calculamos sua decomposição em valores singulares (SVD) para obter $F = U S V^{T}$, forçamos o último autovalor como 0 e remontamos uma nova matriz fundamental \textbf{F\textquotesingle}.

A segunda estratégia foi o algoritmo de 8 pontos normalizado. Antes de aplicar o algoritmo de 8 pontos, cada par de ponto é multiplicado por uma matriz para ser transladado e escalado de modo que suas coordenadas caiam no intervalo $[-1,1]$, ou seja,
\begin{equation}
\hat{x_{i}} = T x_{i}\text{ e }\hat{x_{i}\textquotesingle} = T x_{i}\textquotesingle.
\end{equation}
Em seguida, calculamos a matriz fundamental $\hat{F}$ com o algoritmo de 8 pontos, obtemos $\hat{F}\textquotesingle$ de rank corrigido e a desnormalizamos para obter \textbf{F}, da forma:
\begin{equation}
F = T\textquotesingle^{T}\hat{F}\textquotesingle T
\end{equation}

A terceira estratégia foi aplicar um descritor de características invariantes nas duas imagens (no caso, ASIFT) para obter vários pares de correspondência e estimar F utilizando a RANSAC, baseada no algoritmo de 7 pontos, por sua vez.

Por fim, tentamos estimar o ponto da cena \textbf{X}. Para cada ponto \textbf{x} na primeira imagem, calculamos sua linha epipolar \textbf{l\textquotesingle} e procuramos nela por uma correspondência usando soma do quadrado das diferenças (SSD). Em seguida, formulamos as projeções \textbf{P} e \textbf{P\textquotesingle} conforme as Equações~\ref{eq:p} e ~\ref{eq:p_}. Por fim, utilizamos triangulação linear para estimar \textbf{X}. Sabemos que
\begin{equation}
x \times (P X) = 0 \text{ e } x\textquotesingle \times (P X\textquotesingle) = 0.
\end{equation}
Após manipulação algébrica, obtemos uma expressão $A X = 0$ onde
\begin{equation}
A = \begin{bmatrix}
x p^{3T} - p^{1T} \\ 
y p^{3T} - p^{2T}\\ 
x\textquotesingle p\textquotesingle^{3T} - p\textquotesingle^{1T}\\ 
y\textquotesingle p\textquotesingle^{3T} - p\textquotesingle^{2T}
\end{bmatrix}.
\end{equation}
Por SVD, descobrimos os coeficientes do ponto \textbf{X}.

\section{Resultados}
%distâncias epipolares
%8 pts: 26/29
%8 norm: 17/18
%ransac: 38/35

A seguir, mostramos os resultados obtidos. Utilizamos, dentre outros, o par de imagens do \textit{dataset} exibido na Figura~\ref{fig:temple}.

\begin{figure}[h]
\centering     %%% not \center
\subfigure[]{\includegraphics[width=60mm]{../tests/temple/temple0102.png}}
\subfigure[]{\includegraphics[width=60mm]{../tests/temple/temple0107.png}}
\caption{Par de imagens usado para os testes.}
\label{fig:temple}
\end{figure}

Com o algoritmo de 8 pontos, conseguimos uma distância média quadrática entre cada ponto e sua linha epipolar em torno de 26 pixels na primeira imagem e 29 na segunda. As linhas epipolares da segunda imagem (em vermelho) estão exibidas na Figura~\ref{fig:ep_lines_8pts}. Os pontos verdes são os marcados pelo usuário.

\begin{figure}[h]
\centering     %%% not \center
\subfigure[Algoritmo de 8 pontos.]{\label{fig:ep_lines_8pts} \includegraphics[width=60mm]{imgs/epipolar_8pts.jpg}}
\subfigure[Algoritmo de 8 pontos normalizado.]{ \label{fig:ep_lines_8norm} \includegraphics[width=60mm]{imgs/epipolar_8norm.jpg}}
\subfigure[RANSAC.]{\label{fig:ep_lines_ransac_ok}\includegraphics[width=60mm]{imgs/epipolar_ransac.jpg}}
\subfigure[RANSAC em caso falho.]{\label{fig:ep_lines_ransac_doido}\includegraphics[width=60mm]{imgs/epipolar_ransac_doido.jpg}}
\caption{Par de imagens usado para os testes.}
\label{fig:temple}
\end{figure}

O algoritmo de 8 pontos normalizado fez as distâncias caírem para 17 na primeira imagem e 18 na segunda, obtendo o restuldado da Figura~\ref{fig:ep_lines_8norm}.

Obtendo os pares de pontos por ASIFT e usando RANSAC para encontrar a matriz fundamental, obtivemos distâncias médias quadráticas de 38 pixels na primeira imagem e 35 na segunda, como mostra a Figura~\ref{fig:ep_lines_ransac_ok}.

Devido ao seu caráter não determinístico, porém, em algumas ocasiões a RANSAC gerou resultados indesejáveis, como na Figura~\ref{fig:ep_lines_ransac_doido}. Acreditamos que isso seja causado por uma tentativa da RANSAC de estimar o epipolo como inifinitamente distante, de modo que as linhas epipolares passem perfeitamente por cada ponto e pelo epipolo.

Alternativamente, capturamos um par de imagens próprias para testes, exibido nas Figuras~\ref{fig:vaca_a} e~\ref{fig:vaca_b}. Houve remoção da distorção radial, mas omitimos essas figuras por serem muito próximas das originais. A calibração da câmera gerou a matriz de parâmetros intrinsicos
\begin{equation}
K = \begin{bmatrix}
702.7 & 0 & 319.5\\ 
0.0 & 702.7 & 239.5\\ 
0.0 & 0.0 & 1.0
\end{bmatrix}.
\end{equation}

\begin{figure}[h]
\centering     %%% not \center
\subfigure[]{\label{fig:vaca_a} \includegraphics[width=60mm]{../tests/vaquinha/1.jpg}}
\subfigure[]{ \label{fig:vaca_b} \includegraphics[width=60mm]{../tests/vaquinha/4.jpg}}
\caption{Par de imagens próprias usado para os testes.}
\end{figure}

Nesse caso, utilizamos apenas os pontos obtidos pelo ASIFT em conjunto com RANSAC para encontrar a matriz fundamental. As linhas epipolares estão exibidas na Figura~\ref{fig:vaca_ep}. Obtivemos distâncias médias quadrádicas em torno de 343 pixels na primeira imagem e 214 pixels na segunda.

\begin{figure}[h]
\centering     %%% not \center
\includegraphics[width=60mm]{imgs/vaca_ep.jpg}
\caption{Linhas epipolares obtidas por RANSAC na imagem própria.}
\label{fig:vaca_ep}
\end{figure}

Não obtivemos, porém, bons resultados da triangulação linear. Na maioria dos casos, os pontos reprojetados ficaram coplanares (como na Figura~\ref{fig:temple_reproject}) ou colineares (Figura~\ref{fig:vaquinha_reproject}). Acreditamos que isso se deva a problemas no calculo dos parâmetros extrínsicos da segunda câmera em relação à primeira.

\begin{figure}[h]
\centering     %%% not \center
\includegraphics[width=80mm]{imgs/temple.png}
\caption{Reprojeção dos pontos do templo usando RANSAC.}
\label{fig:temple_reproject}
\end{figure}

\begin{figure}[h]
\centering     %%% not \center
\includegraphics[width=80mm]{imgs/vaquinha.png}
\caption{Reprojeção dos pontos do brinquedo com RANSAC.}
\label{fig:vaquinha_reproject}
\end{figure}

\section{Dificuldades enfrentadas}

Houve dificuldade para captura das imagens próprias adequadas. Foi utilizada uma câmera de celular Samsung Galaxy S i9000b. O primeiro problema disso é o fato de não termos um manual para garantir a precisão da calibração da câmera. Além disso, as fotografias geradas tinham resolução muito alta, o que tornava os algoritmos muito lentos. A alternativa foi diminuir a resolução para um formato VGA. Porém, as fotografias ficaram desfocadas. Para que o foco ficasse corretamente ajustado ao objeto fotografado, era necessário afastar mais a câmera, o que tornava o objeto pouco visível, devido à baixa resolução.

A grande quantidade de pontos e pixels envolvida no problema também exigiu uma abordagem \textit{multithread} em várias partes do trabalho para garantir que os testes fossem rápidos o suficiente para não travar o desenvolvimento.

%\bibliographystyle{sbc}
%\bibliography{sbc-template}

\end{document}
