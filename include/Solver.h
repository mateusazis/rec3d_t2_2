#pragma once

#include "common.h"
#include "InputData.h"

#include <vector>

class Solver {
public:
	virtual ~Solver();

	virtual Eigen::Matrix3d solve(const EVec<Correspondence> & corresponds) = 0;
};

void calculateProjections(Eigen::Matrix<double, 3, 4>* p, Eigen::Matrix<double, 3, 4>* p_, Eigen::Matrix3d F, InputData* input);
