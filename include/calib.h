#include <opencv2/core/core.hpp>

#include <string>

void getCameraParams(cv::Mat* out_cameraMatrix, cv::Mat* out_distCoeffs, std::string configsFile);