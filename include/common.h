#pragma once
#include <utility>
#include <Eigen/Dense>
#include <opencv2/core/core.hpp>

class InputData;

//Typedefs
typedef std::pair<Eigen::Vector2d, Eigen::Vector2d> Correspondence;
typedef Eigen::Matrix<double, 3, 4> Mat34;

typedef std::pair<Eigen::Vector2i, Eigen::Vector2i> Line;

template<typename T>
using EVec = std::vector<T, Eigen::aligned_allocator<T> >;

typedef EVec<Correspondence> CVec;

class Reprojector;
//Functions
Eigen::Matrix3d getEssentialMatrix(Eigen::Matrix3d F, Eigen::Matrix3d K);
Mat34 assembleProjection(Eigen::Matrix3d K, Eigen::Matrix3d R, Eigen::Vector3d t);
void calculateProjections(Mat34* p, Mat34* p_, Eigen::Matrix3d F, InputData* in, Reprojector* r, const CVec & corresponds);
EVec<Correspondence> findMatches(Eigen::Matrix3d F, const cv::Mat* src, const cv::Mat* dest);
double pointLineDistance(const Eigen::Vector3d & point, const Eigen::Vector3d & line);

//Convertions
Eigen::Vector3d toV3(const Eigen::Vector2d & v);
Eigen::Vector2d toV2(const Eigen::Vector3d & v);
Eigen::Vector3d toV3(Eigen::Vector4d v);
Eigen::Matrix3d toM3(const cv::Mat & m);

template <typename T>
cv::Point toPoint(T v){
	return cv::Point(int(v.x()), int(v.y()));
}


void reproject(const CVec* corresps, EVec<Eigen::Vector3d>* X, std::vector<cv::Vec3b>* colors, Mat34 P, Mat34 P_, Reprojector* r, const cv::Mat* img);