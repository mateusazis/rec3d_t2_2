#pragma once

#include "Solver8Points.hpp"

class NormalizedSolver8Points : public Solver8Points {
public:
	NormalizedSolver8Points(int w, int h);
	Eigen::Matrix3d solve(const EVec<Correspondence> & corresponds);

private:
	Eigen::Matrix3d t;
};
