#pragma once

#include "Reprojector.h"

class LinearTriangulator : public Reprojector {
public:
	Eigen::Vector3d reproject(const Eigen::Vector3d & x, const Mat34 & P, const Eigen::Vector3d & x_, const Mat34 & P_);
};

