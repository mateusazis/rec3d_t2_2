#pragma once

#include "Solver.h"

class Solver8Points : public Solver {
public:
	virtual Eigen::Matrix3d solve(const EVec<Correspondence> & corresponds);
};
