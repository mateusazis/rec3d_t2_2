#pragma once

#include "common.h"

bool getEpipolarLine(Eigen::Vector2d x, Eigen::Matrix3d F, Line* out, int width, int height);

bool drawEpipolarLine(Eigen::Vector2d x, Eigen::Matrix3d F, cv::Mat & m);

enum Selection { FIRST, SECOND };
double computeEpipolarDistance(const Eigen::Matrix3d & F, const CVec & v, Selection s);