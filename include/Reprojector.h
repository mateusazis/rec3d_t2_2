#pragma once

#include <Eigen/Dense>

#include "../include/common.h"

class Reprojector {
public:
	virtual ~Reprojector();
	virtual Eigen::Vector3d reproject(const Eigen::Vector3d & x, const Mat34 & P, const Eigen::Vector3d & x_, const Mat34 & P_) = 0;
};
