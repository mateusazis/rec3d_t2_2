#pragma once

#include <vector>
#include <Eigen/Core>
#include <Eigen/StdVector>
#include <utility>
#include <string>
#include <limits>

#include "common.h"

namespace cv{
	class Mat;
}

typedef EVec<Correspondence> CorrespondVec;

class InputData {
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	cv::Mat images[2];
	CorrespondVec relations;
	Eigen::Matrix3d K; //intrinsic parameters matrix
	Mat34 p, p_; //intrinsic parameters matrix

	static InputData* fromCorrespondences(std::string filePath);
	static InputData* fromASIFT(std::string filePaths);
	static InputData* fromCustomCamera(std::string filePath, bool cached = false, std::string configFile = "in_VID5.xml");

private:
	InputData(std::string filePath);
	std::vector<std::string> paths;
};
