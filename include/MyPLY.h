#pragma once

#include <vector>
#include <Eigen/Dense>
#include <opencv2/core/core.hpp>

#include "common.h"

void writeToPLY(const char* outFile, const EVec<Eigen::Vector3d> & vertices, const std::vector<cv::Vec3b> & colors);
void plot(const EVec<Eigen::Vector3d> & vertices, const std::vector<cv::Vec3b> & colors);
