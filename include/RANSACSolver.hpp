#pragma once

#include "Solver8Points.hpp"

class RANSACSolver : public Solver {
public:
	RANSACSolver(double trustness, double pixelTolerance);
	Eigen::Matrix3d solve(const EVec<Correspondence> & corresponds);

private:
	double trustness, pixelTolerance;

	void findInliers(const EVec<Correspondence> & corresponds,
			Eigen::Matrix3d F,
			EVec<Correspondence> & inliers);
};
