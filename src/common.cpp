#include "../include/common.h"
#include "../include/InputData.h"
#include "../include/Reprojector.h"

#include <opencv2/imgproc/imgproc.hpp>

#include <iostream>

#define CHEATED_EXTRINSIC_PARAMS

static const int SSD_WINDOW_SIZE = 15, MIN_LUMINANCE = 20;

using namespace Eigen;
using namespace std;
using namespace cv;

Matrix3d getEssentialMatrix(Matrix3d F, Matrix3d K){
	return K.transpose() * F * K;
}

Mat34 assembleProjection(Matrix3d K, Matrix3d R, Vector3d t){
	Mat34 tMat;
	tMat.col(0) << R.col(0);
	tMat.col(1) << R.col(1);
	tMat.col(2) << R.col(2);
	tMat.col(3) << t;
	return K * tMat;

	/*Mat34 tMat = Mat34::Identity();
	tMat.col(3) << t;
	return K * R * tMat;*/
}

static Vector3d fromCrossmat(const Matrix3d & m){
	JacobiSVD<Matrix3d> svd = m.jacobiSvd(ComputeFullV);
	MatrixXd V = svd.matrixV();
	return V.col(V.cols() - 1);
}


void calculateProjections(Mat34* p, Mat34* p_, Matrix3d F, InputData* in, Reprojector* r, const CVec & corresponds){
	Matrix3d K, U, V, W, Z, S1, S2, R1, R2, R;

#ifdef CHEATED_EXTRINSIC_PARAMS
	*p = in->p;
	*p_ = in->p_;
#else
	K = in->K;
	Mat34 P = assembleProjection(K, Matrix3d::Identity(), Vector3d::Zero());
	*p = P;



	Matrix3d E = K.transpose() * F * K;

	JacobiSVD<Matrix3d> svd = E.jacobiSvd(ComputeFullU | ComputeFullV);
	U = svd.matrixU();
	V = svd.matrixV();
//	cout << "U\n" << U << endl;
//	cout << "S\n" << svd.singularValues() << endl;
//	cout << "V\n" << svd.matrixV() << endl;
	Vector3d t_1 = U.col(U.cols()-1);
	t_1.normalize();
	Vector3d t_2 = -t_1;

	W <<
		0, -1, 0,
		1, 0, 0,
		0, 0, 1;

	Z <<
		0, 1, 0,
		-1, 0, 0,
		0, 0, 0;

	//Matrix3d tX;
	//tX <<
	//Vector3d t_1, t_2;
	//t_1 = fromCrossmat(V * W * svd.singularValues().asDiagonal() * V.transpose());
	//t_2 = fromCrossmat(V * Z * V.transpose());
	//R = U * W.inverse() * V.transpose();

	cout << "t_1:\n" << t_1 << endl;
	cout << "t_2:\n" << t_2 << endl;
	//cout << "R:\n" << R << endl;



	S1 = -U * Z * U.transpose();
	R1 = U * W.transpose() * V.transpose();
	S2 = U * Z * U.transpose();
	R2 = U * W * V.transpose();

	cout << "R_1:\n" << R1 << endl;
	cout << "R_2:\n" << R2 << endl;

	Mat34 P_1 = assembleProjection(K, R1, t_1),
		P_2 = assembleProjection(K, R1, t_2),
		P_3 = assembleProjection(K, R2, t_1),
		P_4 = assembleProjection(K, R2, t_2);

	*p_ = P_3;
	

	cout << "Results of projection calculation:" << endl;
	const Correspondence & c = corresponds[0];
	
	cout << "Test1:\n" << c.first << endl;
	cout << "R1, t1\n" << r->reproject(toV3(c.first), *p, toV3(c.second), P_1) << endl;;
	cout << "R1, t2\n" << r->reproject(toV3(c.first), *p, toV3(c.second), P_2) << endl;;
	cout << "R2, t1\n" << r->reproject(toV3(c.first), *p, toV3(c.second), P_3) << endl;;
	cout << "R2, t2\n" << r->reproject(toV3(c.first), *p, toV3(c.second), P_4) << endl;;

	
	//*p_ = assembleProjection(K, R1, t_1);
	//*p_ = assembleProjection(K, R1, t_2);
	//*p_ = assembleProjection(K, R2, t_1);
	//*p_ = assembleProjection(K, R2, t_2);
#endif
}

Vector3d toV3(const Eigen::Vector2d & v){
	return Vector3d(v.x(), v.y(), 1);
}

Vector2d toV2(const Eigen::Vector3d & v){
	return Vector2d(v.x() / v[2], v.y() / v[2]);
}

Vector3d toV3(Vector4d v){
	v /= v[3];
	return Vector3d(v.x(), v.y(), v.z());
}

Matrix3d toM3(const Mat & m){
	Matrix3d resp;
	resp <<
		m.at<double>(0, 0), m.at<double>(0, 1), m.at<double>(0, 2),
		m.at<double>(1, 0), m.at<double>(1, 1), m.at<double>(1, 2),
		m.at<double>(2, 0), m.at<double>(2, 1), m.at<double>(2, 2);
	return resp;
}

static bool valid(const Point & p, const Mat* m){
	return p.x >= 0 && p.y >= 0 && p.x < m->cols && p.y < m->rows;
}

bool getEpipolarLine(Eigen::Vector2d x, Eigen::Matrix3d F, Line* out, int width, int height);

static bool ssdFind(const Vector2d & x, const Matrix3d & F, Vector2d* out, const Mat* src, const Mat* dest, int windowSize){
	Line l;
	if(getEpipolarLine(x, F, &l, dest->cols, dest->rows)){
		Point start = toPoint(l.first);
		Point end = toPoint(l.second);
		Point xP = toPoint(x);
		LineIterator it(*dest, start, end);



		const int pos = windowSize / 2;
		const int pre = -pos;
		double bestSum = numeric_limits<double>::max();
		Point best;

		for(int i = 0; i < it.count; i++, ++it){
		    Point p = it.pos();

			double sum = 0;
			for (int m = pre; m <= pos; m++){
				for (int n = pre; n <= pos; n++){
					Point pNew(p.x + m, p.y + n);
					Point pOld(xP.x + m, xP.y + n);
					if (valid(pNew, dest) && valid(pOld, src)){
						Vec3b a = src->at<Vec3b>(pNew);
						Vec3b b = dest->at<Vec3b>(pOld);

						Vector2d c1(a[1], a[2]), 
							c2(b[1], b[2]);
						double diff = (c1 - c2).norm();

						sum += diff * diff;
					}

				}
			}
			if (sum < bestSum){
				bestSum = sum;
				best = p;
			}
		}
		if (bestSum < 100000)
		{
			//cout << "Sum: " << bestSum << endl;
			*out = Vector2d(best.x, best.y);
			return true;
		}
	}
	return false;
}

static void parallel_findMatches(Matrix3d F, const Mat* src, const Mat* dest, int startY, int endY, CVec* v){
	cout << "[Threaded] Looking for SSD matches...\n";

	Mat labSRC, labDEST;
	cvtColor(*src, labSRC, CV_BGR2Lab);
	cvtColor(*dest, labDEST, CV_BGR2Lab);

	EVec<Correspondence> resp;
	for(int y = startY; y < endY; y++){
		for(int x = 0; x < src->cols; x++){
			Vector2d p(x, y);
			Vector2d p_;
			unsigned char luminance_src = src->at<Vec3b>(y, x)[0];
			unsigned char luminance_dest = dest->at<Vec3b>(y, x)[0];
			bool brightEnough = luminance_src >= MIN_LUMINANCE && luminance_dest >= MIN_LUMINANCE;
			if(brightEnough && ssdFind(p, F, &p_, &labSRC, &labDEST, SSD_WINDOW_SIZE)){
				v->push_back(make_pair(p, p_));
			}
		}
	}

	cout << "Thread got " << v->size() << " matches!\n";
}
#include <thread>

EVec<Correspondence> findMatches(Matrix3d F, const Mat* src, const Mat* dest){
	cout << "Looking for SSD matches..." << endl;
	const int THREAD_COUNT = 8;
	int lines_per_thread = src->rows / THREAD_COUNT;


	CVec v[THREAD_COUNT];
	thread *t[THREAD_COUNT];
	for(int i = 0; i < THREAD_COUNT; i++){
		int startY = lines_per_thread * i;
		int endY = i == THREAD_COUNT - 1 ? src->rows : lines_per_thread * (i+1);
		t[i] = new thread(parallel_findMatches, F, src, dest, startY, endY, v+i);
	}

	CVec resp;
	for(int i = 0; i < THREAD_COUNT; i++){
		t[i]->join();
		delete t[i];
		resp.insert(resp.end(), v[i].begin(), v[i].end());
	}
	cout << "SSD done!" << endl;

	return resp;
}

double pointLineDistance(const Eigen::Vector3d & point, const Eigen::Vector3d & line){
	double sub = sqrt(line[0] * line[0] + line[1] * line[1]);
	return abs(line.dot(point)) / sub;
}

static void parallel_Reproject(const CVec* corresps, EVec<Vector3d>* X, vector<Vec3b>* colors, Mat34 P, Mat34 P_, Reprojector* r, const Mat* img, int start, int end){
	EVec<Correspondence> resp;
	for (int i = start; i < end; i++){
		Vector2d x = corresps->at(i).first;
		Vector2d x_ = corresps->at(i).second;

		Vector3d x3 = toV3(x);
		Vector3d x3_ = toV3(x_);

		Vector3d reprojected = r->reproject(x3, P, x3_, P_);
		//if (reprojected.x() < 10000 && reprojected.x() > -10000)
		{
			X->push_back(reprojected);
			Point pixel((int)x[0], (int)x[1]);
			Vec3b color = img->at<Vec3b>(pixel);
			colors->push_back(color);
		}
	}
}

void reproject(const CVec* corresps, EVec<Vector3d>* X, vector<Vec3b>* colors, Mat34 P, Mat34 P_, Reprojector* r, const Mat* img){
	cout << "Reprojecting" << corresps->size() << "points..." << endl;
	const size_t THREAD_COUNT = 8;
	int lines_per_thread = int(corresps->size()) / THREAD_COUNT;

	EVec<Vector3d> parallelX[THREAD_COUNT];
	vector<Vec3b> parallelColors[THREAD_COUNT];

	thread *t[THREAD_COUNT];
	for (int i = 0; i < THREAD_COUNT; i++){
		int start = lines_per_thread * i;
		int end = i == THREAD_COUNT - 1 ? int(corresps->size()) : lines_per_thread * (i + 1);
		t[i] = new thread(parallel_Reproject, corresps, parallelX + i, parallelColors+i, P, P_, r, img, start, end);
	}

	for (int i = 0; i < THREAD_COUNT; i++){
		t[i]->join();
		delete t[i];
		X->insert(X->end(), parallelX[i].begin(), parallelX[i].end());
		colors->insert(colors->end(), parallelColors[i].begin(), parallelColors[i].end());
	}
	cout << "Reprojection done!" << endl;
}