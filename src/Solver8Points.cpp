#include "../include/Solver8Points.hpp"
#include "../include/debug"

using namespace Eigen;
using namespace std;

Solver::~Solver(){}

static Matrix3d fixRank(Matrix3d F){
	JacobiSVD<Matrix3d> svd = F.jacobiSvd(ComputeFullV | ComputeFullU);

	MatrixXd u = svd.matrixU();
	VectorXd s = svd.singularValues();
	MatrixXd v = svd.matrixV();

	s[s.rows() - 1] = 0;

	return u * s.asDiagonal() * v.transpose();
}

Matrix3d Solver8Points::solve(const EVec<Correspondence> & corresponds){
	MatrixXd A = MatrixXd(corresponds.size(), 9);

	for(size_t i = 0; i < corresponds.size(); i++){
		const Correspondence & c = corresponds[i];
		const Vector2d & v = c.first;
		const Vector2d & v_ = c.second;
		A.row(i) << v.x() * v_.x(), v_.x() * v.y(), v_.x(), v_.y() * v.x(), v_.y() * v.y(), v_.y(), v.x(), v.y(), 1;
	}

	//resolver por SVD
	//cout << "Matriz A:\n" << A << endl;

	JacobiSVD<MatrixXd> svd = A.jacobiSvd(ComputeFullV);
	MatrixXd vMatrix = svd.matrixV();

	VectorXd solution = vMatrix.col(vMatrix.cols() - 1);

	solution /= solution[8];

	Matrix3d sol;
	for (int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++)
			sol(i, j) = solution[i * 3 + j];
	sol(2, 2) = 1;

	//sol *= -0.01747546661320564;
	//cout << "Matriz F sem rank\n" << sol << endl;

	return fixRank(sol);
}


