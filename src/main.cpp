#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <Eigen/Dense>
#include <iostream>

#include "../include/Solver8Points.hpp"
#include "../include/Normalized8PointsSolver.hpp"
#include "../include/RANSACSolver.hpp"
#include "../include/InputData.h"
#include "../include/LinearTriangulator.h"
#include "../include/IterativeTriangulator.h"
#include "../include/MyPLY.h"
#include "../include/Epipolar.h"

using namespace std;
using namespace Eigen;
using namespace cv;

void drawEpipolarLines(InputData *d, Matrix3d f){
	for (Correspondence & c : d->relations){
		const Vector2d & v = c.second;
		Point p(v.x(), v.y());
		circle(d->images[1], p, 5, Scalar(0, 255, 0, 255), -1);
		circle(d->images[1], p, 5, Scalar(0, 0, 0, 255), 2);
	}


	for (int i = 0; i < d->relations.size(); i++){
		//for (int i = 0; i < 2; i++){
		Correspondence & c = d->relations[i];
		drawEpipolarLine(c.first, f, d->images[1]);
		//break;
	}

	

	Mat & img = d->images[1];
	//resize(img, img, Size(img.cols/2, img.rows/2));
	imshow("", d->images[1]);
	waitKey(0);
	destroyAllWindows();
	imwrite("epipolar.jpg", d->images[1]);
	exit(0);
}

static void parseInput(InputData **d, Solver** s, int argc, char** argv){
	vector<string> args(argv, argv + argc);
	vector<string>::iterator it;

	it = find(args.begin(), args.end(), "-srcimg");
	string src = it[1];
	if (src == "dataset"){
		it = find(args.begin(), args.end(), "-srcpts");
		string src = it[1];
		if (src == "manual")
			*d = InputData::fromCorrespondences(argv[1]);
		else if (src == "auto")
			*d = InputData::fromASIFT(argv[1]);
	}
	else //src == custom
		*d = InputData::fromCustomCamera(argv[1]);

	it = find(args.begin(), args.end(), "-method");
	string method = it[1];
	if (method == "8pts")
		*s = new Solver8Points();
	else if (method == "8ptsN")
		*s = new NormalizedSolver8Points((*d)->images[0].cols, (*d)->images[0].rows);
	else{ //method == "ransac"
		double trustness = atof(it[2].data());
		double tolerance = atof(it[3].data());
		*s = new RANSACSolver(trustness, tolerance);
	}
}

int main(int argc, char** argv){
	InputData* d;
	Solver* s;
	parseInput(&d, &s, argc, argv);

	Matrix3d f = s->solve(d->relations);
	delete s;
	
	cout << fixed << f << endl;

	Reprojector *r = new LinearTriangulator();

	Mat34 P, P_;
	calculateProjections(&P, &P_, f, d, r, d->relations);

	cout << "P\n" << P << endl;
	cout << "P\'\n" << P_ << endl;

	cout << "Epipolar dist 0: " << computeEpipolarDistance(f, d->relations, FIRST) << endl;
	cout << "Epipolar dist 1: " << computeEpipolarDistance(f, d->relations, SECOND) << endl;

	//drawEpipolarLines(d, f);

	EVec<Correspondence> points = findMatches(f, d->images, d->images + 1);
	//const EVec<Correspondence> & points = d->relations;
	
	Vector3d vvv = r->reproject(Vector3d(16, 456, 1), P, Vector3d(71, 505, 1), P_);

	//r = new IterativeTriangulator(points);
	vector<Vec3b> colors;
	EVec<Vector3d> X;
	//r = new IterativeTriangulator(d->relations);
	reproject(&points, &X, &colors, P, P_, r, d->images);

	plot(X, colors);
	//writeToPLY("out.ply", X, colors);

	delete r;
	delete d;

	return 0;
}
