#include "../include/Epipolar.h"

#include <iostream>
#include <limits>

using namespace Eigen;
using namespace std;
using namespace cv;

static inline double getY(double A, double B, double C, double x){
	return (-C - A*x) / B;
}

bool getEpipolarLine(Vector2d x, Matrix3d F, Line* out, int width, int height){
	Vector3d newX(x.x(), x.y(), 1);
	Vector3d l = F * newX;

	double A = l[0], B = l[1], C = l[2];
	double x0 = 0, xF = width - 1;

	*out = make_pair(Vector2i(x0, getY(A, B, C, x0)), Vector2i(xF, getY(A, B, C, xF)));
	return true;
}

bool drawEpipolarLine(Vector2d x, Matrix3d F, cv::Mat & m){
	Line l;
	if (getEpipolarLine(x, F, &l, m.cols, m.rows)){
		Point a(l.first.x(), l.first.y());
		Point b(l.second.x(), l.second.y());

		line(m, a, b, Scalar(0, 0, 255, 255));
	}
	return false;
}

double computeEpipolarDistance(const Matrix3d & F, const CVec & relations, Selection s){
	double sum = 0;
	for (const Correspondence & c : relations){
		Vector3d epipolarLine, p;
		if (s == SECOND){
			epipolarLine = F * toV3(c.first);
			p = toV3(c.second);
		}
		else{
			epipolarLine = toV3(c.second).transpose() * F;
			p = toV3(c.first);
		}
		
		double dist = pointLineDistance(p, epipolarLine);
		sum += dist * dist;
	}

	return sqrt(sum / relations.size());
}