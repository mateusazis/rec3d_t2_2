#include "../include/LinearTriangulator.h"
#include "../include/common.h"
#include "../include/debug"
#include <opencv2/core/core.hpp>
using namespace Eigen;
using namespace cv;
using namespace std;

Reprojector::~Reprojector(){}

Vector3d LinearTriangulator::reproject(const Vector3d & x, const Mat34 & P, const Vector3d & x_, const Mat34 & P_){

	Matrix4d A;
	A <<	RowVector4d(x.x()  * P.row(2).transpose() -  P.row(0).transpose()),
			RowVector4d(x.y()  * P.row(2).transpose() -  P.row(1).transpose()),
			RowVector4d(x_.x() * P_.row(2).transpose() - P_.row(0).transpose()),
			RowVector4d(x_.y() * P_.row(2).transpose() - P_.row(1).transpose());
	
	Matrix4d B = A.transpose();
	Mat svdMat(4, 4, CV_64F, B.data()), s, u, vt;
	cv::SVD::compute(svdMat, u, s, vt, cv::SVD::FULL_UV);
	//A.transposeInPlace();
	//std::cout << A << std::endl;
	//JacobiSVD<Matrix4d> svd = JacobiSVD<Matrix4d>(A, ComputeFullV);
	//MatrixXd v = svd.matrixV();

	////Vector4d solution = v.col(v.cols() - 1);
	//Vector4d solution = v.row(v.rows() - 1);
	cv::Mat point3dTmp = vt.col(vt.cols - 1);

	Vector3d resp(point3dTmp.at<double>(0, 0), point3dTmp.at<double>(1, 0), point3dTmp.at<double>(2, 0));
	return resp / point3dTmp.at<double>(3, 0);
	//cout << "SVD Mat:\n" << svdMat << endl;
	//cout << "CV resp:\n" << point3dTmp << "\nEigen resp::\n" << solution << endl;


	// std::cout << "Non-normalized resp:\n" << solution << std::endl;
	//return toV3(solution);
}
