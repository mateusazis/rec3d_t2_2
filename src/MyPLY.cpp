#include "../include/MyPLY.h"

#include <fstream>
#include <cstdio>
#include <cstdlib>

using namespace std;
using namespace Eigen;
using namespace cv;

void writeToPLY(const char* outFile, const EVec<Vector3d> & vertices, const vector<Vec3b> & colors){
	fstream o;
	o.open(outFile, fstream::out);

	o << "ply" << endl;
	o << "format ascii 1.0" << endl;
	o << "element vertex " << vertices.size() << endl;
	o << "property float x" << endl;
	o << "property float y" << endl;
	o << "property float z" << endl;
	o << "property uchar red" << endl;
	o << "property uchar green" << endl;
	o << "property uchar blue" << endl;
	o << "end_header";

	o.precision(7);
	for(size_t i = 0; i < vertices.size(); i++){
		const Vector3d & v = vertices[i];
		const Vec3b & c = colors[i];
		o << endl << fixed << v.x() << ' ' << fixed << v.y() << ' ' << fixed << v.z();
		o << endl << (int)c[2] << ' ' << (int)c[1] << ' ' << (int)c[0];
	}

	o.close();
}

void plot(const EVec<Vector3d> & vertices, const vector<Vec3b> & colors){
	const char* tempFileName = "__temp.ply";
	writeToPLY(tempFileName, vertices, colors);
	char cmd[100] = "";
	sprintf(cmd, "\"C:/Program Files/VCG/MeshLab/meshlab.exe\" %s", tempFileName);
	system(cmd);
	remove(tempFileName);
}
