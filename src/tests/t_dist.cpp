#include"../../include/Epipolar.h"
#include <iostream>
#include <Eigen/Dense>

using namespace Eigen;
using namespace std;

int main(){
	Vector3d point(4, 4, 1),
		line(1, 1, 0);
	cout << pointLineDistance(point, line) << endl;

	return 0;
}