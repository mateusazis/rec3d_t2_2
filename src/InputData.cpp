#include "../include/InputData.h"
#include "../include/json.h"
#include "../include/calib.h"

#include <JsonBox.h>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/core.hpp>
#include <demo_asift.h>

using namespace std;
using namespace JsonBox;
using namespace cv;

static CorrespondVec buildCorresp(const Array & a, const Array & b){
	using Eigen::Vector2d;
	CorrespondVec corresp(a.size() / 2);
	for (size_t j = 0; j < a.size(); j += 2){
		Vector2d vA((double)a[j].getInt(), (double)a[j + 1].getInt());
		Vector2d vB((double)b[j].getInt(), (double)b[j + 1].getInt());
		Correspondence c = Correspondence(vA, vB);
		corresp[j/2] = c;
	}
	return corresp;
}

static double toDouble(const Value & v){
	int intVal = v.getInt();
	if (intVal != 0)
		return intVal;
	return v.getDouble();
}

static Eigen::Matrix3d readMatrix(const Array & arr){
	Eigen::Matrix3d R;
	for(int i = 0; i < 3; i++)
		for (int j = 0; j < 3; j++){
			double d = toDouble(arr[3 * i + j]);
			R(i, j) = d;
		}
	return R;
}

static Mat34 getProj(Eigen::Matrix3d K, const Array & arr){
	using namespace Eigen;
	Matrix3d R = readMatrix(arr);
	Vector3d t = Vector3d(toDouble(arr[9]), toDouble(arr[10]), toDouble(arr[11]));

	return assembleProjection(K, R, t);
}

InputData::InputData(string filePath){
	Value data;
	data.loadFromFile(filePath);

	Array paths = data["paths"].getArray();

	for (int i = 0; i < 2; i++){
		string path = paths[i].getString();
		this->paths.push_back(path);
		Mat m = imread(path, CV_LOAD_IMAGE_COLOR);
		this->images[i] = m;
	}



	const Array & kCoefs = data["camera_params"].getArray();
	this->K = readMatrix(kCoefs);

	p = getProj(this->K, data["p1"].getArray());
	p_ = getProj(this->K, data["p2"].getArray());
}

InputData* InputData::fromCorrespondences(std::string filePath){
	InputData* resp = new InputData(filePath);

	Value data;
	data.loadFromFile(filePath);

	Array correspondences = data["correspondences"].getArray();

	Array aCorresp = correspondences[0].getArray();
	Array bCorresp = correspondences[1].getArray();
	resp->relations = buildCorresp(aCorresp, bCorresp);


	return resp;
}

// ASIFT preparation stuff
static std::vector<float> toGray(const Mat& m){
	Mat convert;
	Mat gray(m.rows, m.cols, CV_32FC1);
	m.convertTo(convert, CV_32FC3, 1.0);

	cvtColor(convert, gray, CV_BGR2GRAY, 1);

	int matSize = gray.rows * gray.cols;
	vector<float> v(matSize);
	memcpy(v.data(), gray.ptr<float>(), matSize * sizeof(float));
	return v;
}

static bool fileExists(const char* path){
	FILE* f = fopen(path, "r");
	bool exists = f != NULL;
	if (exists)
		fclose(f);
	return exists;
}


typedef pair<Eigen::Vector2f, Eigen::Vector2f> CorrespF;


CorrespondVec cvt(const vector<CorrespF, Eigen::aligned_allocator<Eigen::Vector2f> > & v){
	cout << "asift generated " << v.size() * 4 << " float entries" << endl;
	using namespace Eigen;
	CorrespondVec resp(v.size());
	for(size_t i = 0; i < v.size(); i++){
		CorrespF c = v[i];
		resp[i] = make_pair(Vector2d(c.first.x(), c.first.y()), Vector2d(c.second.x(), c.second.y()));
	}
	return resp;
}

static CorrespondVec _computeASIFT(const Mat& a, const Mat & b){
	vector<float> v1 = toGray(a), v2 = toGray(b);
	vector<CorrespF, Eigen::aligned_allocator<Eigen::Vector2f> > v = computeMyASIFT(v1, v2, a.cols, a.rows, b.cols, b.rows);
	return cvt(v);
}

// END: ASIFT preparation stuff



//static vector<Correspondence> selectRandomPoints(const vector<Correspondence> & src, int count){
//	if (src.size() < count)
//		return src;
//	vector<int> available(src.size());
//	vector<Correspondence> resp(count);
//	srand(time(NULL));
//	for (int i = 0; i < src.size(); i++)
//		available[i] = i;
//	while (count--){
//		int randIndex = rand() % available.size();
//		resp[count] = src[available[randIndex]];
//		available.erase(available.begin() + randIndex);
//	}
//
//	return resp;
//}

//static CorrespondVec selectNPoints(const CorrespondVec & src, size_t count){
//	if (src.size() < count)
//		return src;
//	return CorrespondVec(src.begin(), src.begin() + count);
//}

static void readFile(const char* file, char* resp){
	resp[0] = '\0';
	char line[1024];
	FILE* f = fopen(file, "r");

	while(fgets(line, 1024, f))
		strcat(resp, line);

	fclose(f);
}

static json_value* json_parse_file(const char* path){
	char* content = (char*)malloc(1024*1024*sizeof(char));
	readFile(path, content);
	json_value* resp = json_parse(content, strlen(content));
	free(content);
	return resp;
}

static bool loadFromCache(CorrespondVec* out, string aPath, string bPath){
	bool found = false;
	if (fileExists("asift_cache.json")){
		json_value* v = json_parse_file("asift_cache.json");

		for (size_t i = 0; !found && i < v->u.array.length; i++){
			json_value *entry = v->u.array.values[i];
			string first = (*entry)["first"].u.string.ptr;
			string second =(*entry)["second"].u.string.ptr;
			bool pathsMatch = (first == aPath && second == bPath) || (first == bPath && second == aPath);
			if (pathsMatch){
				cout << "Loading ASIFT from cache" << endl;
				size_t arrayLen = (*entry)["values"].u.array.length;
				json_value** values = (*entry)["values"].u.array.values;
//				cout << "size of array: " << arrayLen << endl;
				for (size_t j = 0; j < arrayLen; j += 4){
					Eigen::Vector2d a(values[j]->u.dbl, values[j+1]->u.dbl);
					Eigen::Vector2d b(values[j+2]->u.dbl, values[j + 3]->u.dbl);
					out->push_back(make_pair(a, b));
				}
//				cout << "returning array of size: " << out->size() << endl;
				found = true;
			}
		}
		json_value_free(v);
	}
	return found;
}

static CorrespondVec findCorresp(const Mat& a, const Mat & b, string aPath, string bPath){
	CorrespondVec resp;

	if(!loadFromCache(&resp, aPath, bPath)){
		resp = _computeASIFT(a, b);

		Value v2;
		v2["first"] = aPath;
		v2["second"] = bPath;
		JsonBox::Array r_;
		for (size_t i = 0; i < resp.size(); i++){
			Eigen::Vector2d from = resp[i].first;
			Eigen::Vector2d to = resp[i].second;

			r_.push_back(Value(from.x()));
			r_.push_back(Value(from.y()));
			r_.push_back(Value(to.x()));
			r_.push_back(Value(to.y()));
		}
		cout << "writing json of " << r_.size() << " floats" << endl;
		v2["values"] = r_;

		Value v;
		Array r;
		if (fileExists("asift_cache.json")){
			v.loadFromFile("asift_cache.json");
			r = v.getArray();
		}

		r.push_back(v2);
		Value(r).writeToFile("asift_cache.json");
	}
	return resp;
}

InputData* InputData::fromASIFT(std::string filePath){
	InputData* resp = new InputData(filePath);


	Mat & a = resp->images[0],
		& b = resp->images[1];
	resp->relations = findCorresp(a, b, resp->paths[0], resp->paths[1]);

	return resp;
}

InputData* InputData::fromCustomCamera(string filePath, bool cached, string configFile){
	InputData* resp = new InputData(filePath);


	Mat k, d, undistorted0, undistorted1;
	if (!cached)
		getCameraParams(&k, &d, configFile);
	else {
		Value v;
		v.loadFromFile(filePath);
		const Array & r = v["distortion"].getArray();
		
		k = Mat(3, 3, CV_64F);
		d = Mat(5, 1, CV_64F);
		for (int i = 0; i < 5; i++)
			d.at<double>(i, 0) = toDouble(r[i]);
		const Array & k_arr = v["camera_params"].getArray();
		for (int i = 0; i < 9; i++)
			k.at<double>(i / 3, i % 3) = toDouble(k_arr[i]);
	}
	undistort(resp->images[0], undistorted0, k, d);
	undistort(resp->images[1], undistorted1, k, d);
	
	resp->images[0] = undistorted0;
	resp->images[1] = undistorted1;
	resp->K = toM3(k);
	
	cout << "\nK:\n" << resp->K << endl;
	cout << "\nD:\n" << d << endl;

	//Do ASIFT after radial distortion correction
	resp->relations = findCorresp(resp->images[0], resp->images[1], resp->paths[0], resp->paths[1]);

	return resp;
}