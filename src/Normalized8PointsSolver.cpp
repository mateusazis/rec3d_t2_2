#include "../include/Normalized8PointsSolver.hpp"

using namespace std;
using namespace Eigen;

NormalizedSolver8Points::NormalizedSolver8Points(int w, int h) : Solver8Points(){
	Matrix3d translation, scale;
	translation <<
		1, 0, -w / 2.0,
		0, 1, -h / 2.0,
		0, 0, 1.0f;
	scale <<
		1, 0, 0,
		0, 1, 0,
		0, 0, (w + h);
	this->t = scale * translation;
}

Eigen::Matrix3d NormalizedSolver8Points::solve(const EVec<Correspondence> & corresponds){
	EVec<Correspondence> v;
	typedef EVec<Correspondence>::const_iterator vec_it;

	for(vec_it it = corresponds.begin(); it < corresponds.end(); it++){
		const Correspondence & c = *it;
		Vector3d first = toV3(c.first);
		Vector3d second = toV3(c.second);

		v.push_back(make_pair(toV2(t * first), toV2(t * second)));

	}

	Matrix3d F = Solver8Points::solve(v);
	F =  t.transpose() * F * t;
	return F / F(2, 2);
}
