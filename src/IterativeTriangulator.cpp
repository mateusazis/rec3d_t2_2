#include "../include/IterativeTriangulator.h"
#include "../include/LinearTriangulator.h"

#include <unsupported/Eigen/NonLinearOptimization>
#include <unsupported/Eigen/NumericalDiff>

#include <iostream>

using namespace Eigen;
using namespace std;

// Generic functor
template<typename _Scalar, int NX = Dynamic, int NY = Dynamic>
struct Functor
{
	typedef _Scalar Scalar;
	enum {
		InputsAtCompileTime = NX,
		ValuesAtCompileTime = NY
	};
	typedef Matrix<Scalar, InputsAtCompileTime, 1> InputType;
	typedef Matrix<Scalar, ValuesAtCompileTime, 1> ValueType;
	typedef Matrix<Scalar, ValuesAtCompileTime, InputsAtCompileTime> JacobianType;

	int m_inputs, m_values;

	Functor() : m_inputs(InputsAtCompileTime), m_values(ValuesAtCompileTime) {}
	Functor(int inputs, int values) : m_inputs(inputs), m_values(values) {}

	int inputs() const { return m_inputs; }
	int values() const { return m_values; }

};

//static Matrix3f toMatrix(VectorXf v){
//	return Matrix3f(v.data()).transpose();
//}
//
//static VectorXf toVector(Matrix3f m){
//	return VectorXf(Map<VectorXf>(m.data(), m.size()));
//}

static VectorXd toVX(const Vector3d & v){
	VectorXd resp(4);
	resp << v.x(), v.y(), v.z(), 1;
	return resp;
}

static Vector3d toV3(const VectorXd & v){
	return Vector3d(v.x(), v.y(), v.z()) / v[3];
}

static Vector4d toV4(const Vector3d & v){
	return Vector4d(v.x(), v.y(), v.z(), 1);
}

static double geomDist(const Vector2d &  a, const Vector2d &  b){
	return (a - b).norm();
}

static double algDist(const Vector2d & a, const Vector2d & b){
	return a.x() * b.y() - a.y() * b.x();
}

typedef double(*DistFunc)(const Vector2d & a, const Vector2d & b);

struct MyFunctor : Functor<double>
{
	MyFunctor(const CVec & inliers, Mat34 P, Mat34 P_) :
		Functor<double>(4, inliers.size()), inliers(inliers), P(P), P_(P_){}

	int operator()(const Eigen::Vector4d &X, Eigen::VectorXd &fvec) const
	{


		DistFunc distFunc;
		distFunc = algDist;
		distFunc = geomDist;


		for (size_t i = 0; i < inliers.size(); i++){
			Vector2d expected_x = inliers[i].first;
			Vector2d expected_x_ = inliers[i].second;
			Vector2d actual_x = toV2(P * X);
			Vector2d actual_x_ = toV2(P_ * X);

			float d1 = distFunc(expected_x, actual_x),
				d2 = distFunc(expected_x_, actual_x_);

			fvec(i) = d1 * d1 + d2 * d2;
		}

		return 0;
	}

	int inputs() const { return 4; } // There are two parameters of the model
	int values() const { // The number of observations
		return inliers.size();
	}

private:
	CVec inliers;
	Mat34 P, P_;
};


IterativeTriangulator::IterativeTriangulator(const CVec & v) : Reprojector(), inliers(v){}

Vector3d IterativeTriangulator::reproject(const Vector3d & x, const Mat34 & P, const Vector3d & x_, const Mat34 & P_){
	//cout << "Levenberg-Marquardt minimization using " << inliers.size() << " inliers." << endl;

	//h.transposeInPlace();
	//esse H agora � o X
	Vector3d X = LinearTriangulator().reproject(x, P, x_, P_);
	VectorXd X4 = toVX(X);

	MyFunctor functor(inliers, P, P_);
	NumericalDiff<MyFunctor> numDiff(functor);
	LevenbergMarquardt<NumericalDiff<MyFunctor>, double> lm(numDiff);
	lm.parameters.maxfev = 2000;
	lm.parameters.xtol = 1.0e-10;

	lm.minimize(X4);
//	std::cout << "iterations: " << lm.iter << std::endl;
//	std::cout << "LVM from:\n" << X << std::endl << "to\n" << toV3(X4) << std::endl;
	return toV3(X4);
}
