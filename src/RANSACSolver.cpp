#include "../include/RANSACSolver.hpp"
#include "../include/Solver8Points.hpp"

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <cmath>
#include <deque>

#include <Eigen/Dense>
#include <unsupported/Eigen/Polynomials>

using namespace Eigen;
using namespace std;


static Matrix3d toMatrix(VectorXd v){
//	Matrix3d resp;
//	resp <<
//		v[0], v[1], v[2],
//		v[3], v[4], v[5],
//		v[6], v[7], v[8];
//	resp << v;
//	return resp;
	return Matrix3d(Map<Matrix3d>(v.data())).transpose();
//	return Matrix3d();
}

static void sumCoefs(Matrix3d f, Matrix3d F, Vector2i x, Vector2i y, Vector2i z, double coefs[4], double multiplier = 1){
	double FX = F(x.x(), x.y());
	double FY = F(y.x(), y.y());
	double FZ = F(z.x(), z.y());

	double dX = f(x.x(), x.y()) -F(x.x(), x.y());
	double dY = f(y.x(), y.y()) -F(y.x(), y.y());
	double dZ = f(z.x(), z.y()) -F(z.x(), z.y());

	coefs[0] += multiplier * dX * dY * dZ;
	coefs[1] += multiplier * (dX * dY * FZ + (dX * FY + dY * FX) * dZ);
	coefs[2] += multiplier * ((dX * FY + dY * FX) * FZ + FX * FY * dZ);
	coefs[3] += multiplier * (FX * FY * FZ);
}

static void solveBy7Points(const EVec<Correspondence> & corresponds, Matrix3d* resp, size_t* respCount){
	MatrixXd A = MatrixXd(7, 9);

	for(size_t i = 0; i < 7; i++){
		const Vector2d & p = corresponds[i].first;
		const Vector2d & p_ = corresponds[i].second;
		double x = p.x(), y = p.y(), x_ = p_.x(), y_ = p_.y();
		A.row(i) << x_*x, x_*y, x_, y_*x, y_*y, y_, x, y, 1;
	}

	JacobiSVD<MatrixXd> svd = A.jacobiSvd(ComputeFullV);
	MatrixXd vMatrix = svd.matrixV();
	VectorXd f2 = vMatrix.col(vMatrix.cols() - 1);
	VectorXd f1 = vMatrix.col(vMatrix.cols() - 2);

	Matrix3d f = toMatrix(f1), F = toMatrix(f2);

	double coefs[] = {0, 0, 0, 0};
	sumCoefs(f, F, Vector2i(0, 0), Vector2i(1, 1), Vector2i(2, 2), coefs);
	sumCoefs(f, F, Vector2i(1, 0), Vector2i(2, 1), Vector2i(0, 2), coefs);
	sumCoefs(f, F, Vector2i(0, 1), Vector2i(1, 2), Vector2i(2, 0), coefs);
	sumCoefs(f, F, Vector2i(0, 2), Vector2i(1, 1), Vector2i(2, 0), coefs, -1);
	sumCoefs(f, F, Vector2i(1, 2), Vector2i(2, 1), Vector2i(0, 0), coefs, -1);
	sumCoefs(f, F, Vector2i(2, 2), Vector2i(0, 1), Vector2i(1, 0), coefs, -1);

	Vector4d coefVector = Vector4d(coefs[3], coefs[2], coefs[1], coefs[0]);

	PolynomialSolver<double, 3> s(coefVector);
	vector<double> roots;
	s.realRoots(roots);
	//cout << "number of real roots: " << roots.size() << endl;

	for(size_t i = 0; i < roots.size(); i++){
		double alpha = roots[i];
		VectorXd solCoefs = alpha * f1 + (1-alpha) * f2;
		resp[i] = toMatrix(solCoefs);
	}
	*respCount = roots.size();
}

static void getRandomPoints(const EVec<Correspondence> & corresponds,
		EVec<Correspondence> & out,
		int selectCount)
{
	int count = (int)corresponds.size();
	deque<int> availableIndices(count);

	int i;
	for (i = 0; i < count; i++)
		availableIndices[i] = i;

	for (i = 0; i < selectCount; i++){
		int randIndex = rand() % availableIndices.size();

		deque<int>::const_iterator it = availableIndices.begin()+ randIndex;
		out[i] = corresponds[*it];
		availableIndices.erase(it);
	}
}

RANSACSolver::RANSACSolver(double trustness, double pixelTolerance) :
		trustness(trustness), pixelTolerance(pixelTolerance){
	srand(unsigned int(time(NULL)));
}

void RANSACSolver::findInliers(
		const EVec<Correspondence> & corresponds,
		Matrix3d F,
		EVec<Correspondence> & inliers){
	for(auto & corresp : corresponds){
		Vector3d x = toV3(corresp.first);
		Vector3d x_ = toV3(corresp.second);
		Vector3d l_ = F * x;
		Vector3d l = x_.transpose() * F;

		if(pointLineDistance(x_, l_) <= pixelTolerance || pointLineDistance(x, l) <= pixelTolerance){
			inliers.push_back(corresp);
		}
	}
}

Eigen::Matrix3d RANSACSolver::solve(const EVec<Correspondence> & corresponds){
	typedef long long int Lint;

	const int INLIERS_PER_SET = 7, MAX_ITERATIONS = 100;
	EVec<Correspondence> chosen(INLIERS_PER_SET), inliers, currentInliers;

	Lint n = numeric_limits<Lint>::max();
	int k = 1;

	Matrix3d estimate[3];
	size_t estimateCount;

//	int s = (int)corresponds.size();
//	Matrix3f m = Matrix3f::Identity();

	do {
		getRandomPoints(corresponds, chosen, INLIERS_PER_SET);
		solveBy7Points(chosen, estimate, &estimateCount);
		Lint oldN = -1;
		for (size_t i = 0; i < estimateCount; i++){
			Matrix3d & estimateF = estimate[i];
			currentInliers.clear();
			findInliers(corresponds, estimateF, currentInliers);
			
			if (currentInliers.size() > inliers.size()){
				inliers = currentInliers;
				double outliersPctg = 1.0 - double(inliers.size()) / corresponds.size();
				//	cout << "Outliers pctg: " << outliersPctg << endl;
				//printf("N from %d", n);
				Lint newN = (Lint)ceil(log(1.0 - trustness) / log(1.0 - pow(1.0 - outliersPctg, INLIERS_PER_SET)));
				if (newN > oldN)
					oldN = newN;
				//printf(" to %d\n", n);
				cout << "n to " << newN << endl;
			}
			
			
		}
		if (oldN != -1)
			n = oldN;


		k++;
	} while (n > k && k < MAX_ITERATIONS);

//	if (outInliers)
//		*outInliers = inliers;

	cout << "RANSAC done with " << k << " iterations." << endl;

	return Solver8Points().solve(inliers);
//	return Matrix3d();
}
